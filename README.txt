Active menu item by path

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

Active menu item by path simplifies Drupal navigation by automatically highlighting active menu items
based on the current path, enhancing user experience.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/active_menu_item_by_path

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/active_menu_item_by_path


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------
The installation of this module is like other Drupal modules,
install the module and enable it.


Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

- navigate to "/admin/config/content/active-menu-settings" to configure menu selection.
- Ensure the "Active menu item by path" permission is granted for access control.
